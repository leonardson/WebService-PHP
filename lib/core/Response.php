<?php

class Response {

    public $data;
    public $code;
    
    public function __construct () {
        $this->data = '{}';
        $this->code = 0;
    }

    /**
     * Imprimirá a resposta no formato JSON
     * TODO: Adicionar outros formatos para resposta
     */
    public function send_response () {
        header("Content-type: application/json; charset=utf-8");
        $response = ['code' => $this->code, 'data' => $this->data];
        
        echo json_encode($response);
        die;
    }

}